clear all
close all
clc
clf

V_low = -7;
 V_high = 1;
 V_div = 9 ; 
 V = logspace(V_low,V_high,V_div); % size range 
%V=[1e-4 4e-1 1e4];
%V=[1e-2 1e-1];
npmax=length(V);
ndef=10;
color_size=jet(9);
xi_pops=[0:0.1:0.9]; % different phytoplankton competing 
ndef=length(xi_pops);



 width = 10;     % Width in inches
  height =10;    % Height in inches
  alw = 4;    % AxesLineWidth
  fsz = 20;      % Fontsize
  lw = 4;      % LineWidth
  
load('Biomass_nodef_seasonal.mat')  
Bnodef_year=Bode(size(Bode,1)-364:size(Bode,1),:);
Bnodef_tot=squeeze(sum(Bnodef_year(:,2:npmax+1),2));

load('Biomass_def_seasonal_addcost.mat')
 Bdef_year=Btime(size(Bode,1)-364:size(Bode,1),:,:);
 Bdef_size=sum(Bdef_year(:,2:npmax+1,:),3);
 Bdef_tot=squeeze(sum(Bdef_size,2));
 for np=1:npmax
    Prop_nodef(:,np)=Bnodef_year(:,np+1)./Bnodef_tot.*100;
    Prop_def(:,np)=Bdef_size(:,np)./Bdef_tot.*100; 
 end
 
 load('Fluxes_nodef_seasonal.mat')
PP_nodef_year=PP_nodef(size(Bode,1)-364:size(Bode,1));
SP_nodef_year=SP_nodef(size(Bode,1)-364:size(Bode,1));
TTE_nodef_year=TTE_nodef(size(Bode,1)-364:size(Bode,1));
mortHTL_nodef_year=mortHTL_nodef(size(Bode,1)-364:size(Bode,1));
mixo_nodef_year=mixotrophy_nodef(size(Bode,1)-364:size(Bode,1),:);

 load('Fluxes_def_seasonal_addcost.mat')
PP_def_year=PP_def(size(Bode,1)-364:size(Bode,1));
SP_def_year=SP_def(size(Bode,1)-364:size(Bode,1));
TTE_def_year=TTE_def(size(Bode,1)-364:size(Bode,1));
mortHTL_def_year=mortHTL_def(size(Bode,1)-364:size(Bode,1));
mixo_def_year=mixotrophy_def(size(Bode,1)-364:size(Bode,1),:,:);

    mixo_def_mean=zeros(365,1);
    mixo_nodef_mean=zeros(365,1);
      mixo_def_size(1:365,npmax)=zeros;
    for np=1:npmax
        mixo_nodef_mean=mixo_nodef_mean+squeeze(mixo_nodef_year(:,np)).*squeeze(Bnodef_year(:,np+1));
       for idef=1:ndef
           mixo_def_size(:,np)=mixo_def_size(:,np)+squeeze(mixo_def_year(:,np,idef)).*squeeze(Bdef_year(:,np+1,idef));
           mixo_def_mean=mixo_def_mean+squeeze(mixo_def_year(:,np,idef)).*squeeze(Bdef_year(:,np+1,idef));
       end
       mixo_def_size(:,np)=mixo_def_size(:,np)./Bdef_size(:,np);
    end
    mixo_nodef_mean=mixo_nodef_mean./Bnodef_tot;
    mixo_def_mean=mixo_def_mean./Bdef_tot;


 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%FIGURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%BIOMASS PER SIZE CLASS
figure(1) 
%subplot(1,2,1)
 padded=padarray((Bdef_size.*5.68.*1e-3).',[1,1],'post');
pcolor(padded)
%contourf(Bnodef_year(:,2:npmax+1).*5.68.*1e-3)
shading flat
colormap(jet)
caxis([0 0.1])
shading interp
c=colorbar
ylabel(c,'Biomass (gC.m^{-3})')
xlabel('Time (days)')
ylabel('Size ((\mug C)')
yticks([1:2:npmax])
%       ij=0;
%       for size_lab=1:1:length(V)
%           ij=ij+1;
%             size_labels(ij,:)=num2str(V(size_lab),'%.1e');
%       end
       size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      yticklabels(size_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
% subplot(1,2,2)
% %contourf(Bdef_size(:,2:npmax+1).*5.68.*1e-3)
% padded=padarray((Bdef_size.*5.68.*1e-3).',[1,1],'post');
% pcolor(padded)
% shading flat
% colormap(jet)
% caxis([0 0.18])
% c=colorbar
% %shading interp
% ylabel(c,'Biomass (gC.m^{-3})')
% xlabel('Time (days)')
% ylabel('Size (\mug C)')
% yticks([1:1:npmax])
%       ij=0;
%       for size_lab=1:1:length(V)
%           ij=ij+1;
%             size_labels(ij,:)=num2str(V(size_lab),'%.1e');
%       end
%       yticklabels(size_labels)


%TOTAL PLANKTON AND INORGANIC NITROGEN
figure(2) 
subplot(1,2,1)
plot(Bnodef_year(:,1)./14,'b')
hold on
plot(Bnodef_tot./14,'g')
subplot(1,2,2)
plot(Bdef_year(:,1,1)./14,'b')
hold on
plot(Bdef_tot./14,'g')

      
%PROPORTION OF EACH SIZE CLASS
figure(3)
subplot(1,2,1)
padded=padarray(Prop_nodef.',[1,1],'post');
pcolor(padded)
shading flat
%shading interp
colormap(jet)
%caxis([0 200])
c=colorbar
ylabel(c,'Biomass (mgC.m^{-3})')
xlabel('Time (days)')
ylabel('Size (\mug C)')
yticks([1:1:npmax])
      ij=0;
      for size_lab=1:1:length(V)
          ij=ij+1;
            size_labels(ij,:)=num2str(V(size_lab),'%.1e');
      end
      yticklabels(size_labels)
subplot(1,2,2)
padded=padarray(Prop_def.',[1,1],'post');
pcolor(padded)
shading flat
%shading interp
colormap(jet)
%caxis([0 200])
c=colorbar
ylabel(c,'Biomass (mgC.m^{-3})')
xlabel('Time (days)')
ylabel('Size (\mug C)')
yticks([1:1:npmax])
      ij=0;
      for size_lab=1:1:length(V)
          ij=ij+1;
            size_labels(ij,:)=num2str(V(size_lab),'%.1e');
      end
      yticklabels(size_labels)
      

       biomass_shannon=Bdef_year;
       biomass_shannon(biomass_shannon<0.001)=0.001;
         Defense=zeros(365,npmax);
         %Shannon_size=zeros(365,npmax);
         Shannon_nodef=zeros(365,1);  
          Shannon_def=zeros(365,1);
           for np=1:npmax
                Shannon_size(1:365,np)=0;
           Shannon_nodef=Shannon_nodef+(Prop_nodef(:,np)./100).*log((Prop_nodef(:,np)./100));
            Shannon_def=Shannon_def+(Prop_def(:,np)./100).*log((Prop_def(:,np)./100));
            for idef=1:ndef
                clear prop
                Defense(:,np)=Defense(:,np)+xi_pops(idef).*Bdef_year(:,np+1,idef);
                prop=biomass_shannon(:,np+1,idef)./Bdef_size(:,np);
                Shannon_size(:,np)=Shannon_size(:,np)+prop.*log(prop);
            end %defense 
            Defense(:,np)=Defense(:,np)./Bdef_size(:,np);
            Shannon_size(:,np)=-Shannon_size(:,np)./log(ndef);
          end
            Shannon_nodef=-Shannon_nodef./log(npmax);
            Shannon_def=-Shannon_def./log(npmax);
            
            
%Size diversity 
figure(4) 
subplot(1,2,1)
plot(Shannon_nodef)
ylim([0.7 1])
subplot(1,2,2)
plot(Shannon_def)
ylim([0.7 1])

%MEAN DEFENSE 
figure(5)
 padded=padarray(Defense.',[1,1],'post');
pcolor(padded)
colormap(jet)
caxis([0 1])
c=colorbar
ylabel(c,'Mean Defense')
xlabel('Time (days)')
ylabel('Size (\mug C)')
yticks([1:2:npmax])
%       ij=0;
%       for size_lab=1:1:length(V)
%           ij=ij+1;
%             size_labels(ij,:)=num2str(V(size_lab),'%.1e');
%       end
%       yticklabels(size_labels)
shading interp
         size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      yticklabels(size_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')

%SHANNON INDEX WITHIN SIZE CLASSES (DIVERSITY IN DEFENSE STRATEGIES)
figure(6)
 padded=padarray(Shannon_size.',[1,1],'post');
pcolor(padded)
shading flat
colormap(jet)
caxis([0 1])
c=colorbar
ylabel(c,'Defense diversity')
xlabel('Time (days)')
ylabel('Size (\mug C)')
yticks([1:2:npmax])
%       ij=0;
%       for size_lab=1:1:length(V)
%           ij=ij+1;
%             size_labels(ij,:)=num2str(V(size_lab),'%.1e');
%       end
%       yticklabels(size_labels)
       shading interp
         size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      yticklabels(size_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
      
 figure(7)
 subplot(2,2,1)
 plot(PP_nodef_year,'g')
 hold on
 plot(PP_def_year,'r')
 ylabel('Primary Production')
 legend('No defense', 'Defense', 'Location', 'Best')
 subplot(2,2,2)
 plot(SP_nodef_year,'g')
 hold on
 plot(SP_def_year,'r')
 ylabel('Secondary Production')
 subplot(2,2,3)
 plot(mortHTL_nodef_year,'g')
 hold on
 plot(mortHTL_def_year,'r')
 ylabel('Flux HTL')
 subplot(2,2,4)
 plot(TTE_nodef_year,'g')
 hold on
 plot(TTE_def_year,'r')
 ylabel('TTE')
 
 figure(8)
 plot(mixo_nodef_mean,'g')
 hold on
 plot(mixo_def_mean,'r')
 ylabel('Mixotrophy')
 legend('No defense', 'Defense', 'Location', 'Best')
 
 figure(9)
%subplot(1,2,1)
 padded=padarray(mixo_def_size.',[1,1],'post');
pcolor(padded)
%contourf(Bnodef_year(:,2:npmax+1).*5.68.*1e-3)
shading flat
colormap(jet)
%caxis([0 200])
shading interp
c=colorbar
ylabel(c,'Mixotrophy)')
xlabel('Time (days)')
ylabel('Size (\mug C)')
yticks([1:2:npmax])
%       ij=0;
%       for size_lab=1:1:length(V)
%           ij=ij+1;
%             size_labels(ij,:)=num2str(V(size_lab),'%.1e');
%       end
 size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      yticklabels(size_labels)
      set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')

% subplot(1,2,2)
% %contourf(Bdef_size(:,2:npmax+1).*5.68.*1e-3)
% padded=padarray(mixo_def_size.',[1,1],'post');
% pcolor(padded)
% shading flat
% colormap(jet)
% %caxis([0 200])
% c=colorbar
% %shading interp
% ylabel(c,'Mixotrophy')
% xlabel('Time (days)')
% ylabel('Size (\mug C)')
% yticks([1:1:npmax])
%       ij=0;
%       for size_lab=1:1:length(V)
%           ij=ij+1;
%             size_labels(ij,:)=num2str(V(size_lab),'%.1e');
%       end
%       yticklabels(size_labels)
 
 
%  fig_name= ['Biomass                 ';...
%             'Tot-Biomass-N           ';...
%             'Proportions             ';...
%             'Shannon-size            ';...
%             'Defense                 ';...
%             'Shannon-Defense         ';...
%             'Fluxes                  ';...
%             'Mixotrophy              ';...
%             'Mixotrophy-size         '];
%      
% 
% for fig=1:9
%     figure(fig)
%     suptitle(fig_name(fig,:))
% %      set(gca,'FontSize',fsz)
% %       set(0,'defaultAxesFontName', 'Arial')
% %     set(0,'defaultTextFontName', 'Arial')
% %   pos = get(gcf, 'Position');
% %   set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
% %   set(gcf,'InvertHardcopy','on');
% %   set(gcf,'PaperUnits', 'inches');
% %   papersize = get(gcf, 'PaperSize');
% %   left = (papersize(1)- width)/2;
% %   bottom = (papersize(2)- height)/2;
% %   myfiguresize = [left, bottom, width, height];
% %   set(gcf,'PaperPosition', myfiguresize);
%   saveas(gcf,fig_name(fig,:),'jpg');
% end
% % 
% %  