clear all 
close all 
clf
clc

%Figures Settings
  width = 10;     % Width in inches
  height =10;    % Height in inches
  alw = 4;    % AxesLineWidth
  fsz = 16;      % Fontsize
  lw = 6;      % LineWidth
  
  V_low = -7;
 V_high = 1;
 V_div = 9 ; 
 V = logspace(V_low,V_high,V_div); % size range 
%V=[1e-4 4e-1 1e4];
%V=[1e-2 1e-1];
npmax=length(V);
xi_pops=[0:0.1:0.9];
ndef=length(xi_pops);

 N0_range=[168 168 14]; % Nitrogen concentration in aphotic layer 100    
 tau_range=[0.1:0.1:1];




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Figures model mixotrophy size-based
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
load('Results_nodef.mat')
load('Fluxes_nodef.mat')
Shannon_nodef=squeeze(Shannon(2:3));
PP_nodef=squeeze(PP(2:3));
SP_nodef=squeeze(SP(2:3));
HTLflux_nodef=squeeze(HTLflux(2:3));
Eff_nodef=squeeze(Eff(2:3));
mixo_nodef=squeeze(mixotrophy(2:3,:));

load('Results_def_Naff.mat')
load('Fluxes_def_Naff.mat')
PP_def_Naff=PP;
SP_def_Naff=SP;
HTLflux_def_Naff=HTLflux;
Eff_def_Naff=Eff;
mixo_def_Naff=mixotrophy;
Defense_Naff=Bdef_all;
Defense_size_Naff=Bdef;
Biomass_tot_Naff=Btot_size.*5.68.*1e-3;
Biomass_size_Naff=Btot.*5.68.*1e-3;
Biomass_Naff=Bnew.*5.68.*1e-3;
Shannon_Naff=Shannon_size;
Shannon_size_Naff=Shannon;


load('Results_def_Naff_F.mat')
load('Fluxes_def_Naff_F.mat')
PP_def_Naff_F=PP;
SP_def_Naff_F=SP;
HTLflux_def_Naff_F=HTLflux;
Eff_def_Naff_F=Eff;
mixo_def_Naff_F=mixotrophy;
Defense_Naff_F=Bdef_all;
Defense_size_Naff_F=Bdef;
Biomass_tot_Naff_F=Btot_size.*5.68.*1e-3;
Biomass_size_Naff_F=Btot.*5.68.*1e-3;
Biomass_Naff_F=Bnew.*5.68.*1e-3;
Shannon_Naff_F=Shannon_size;
Shannon_size_Naff_F=Shannon;

load('Results_def_F.mat')
load('Fluxes_def_F.mat')
PP_def_F=PP;
SP_def_F=SP;
HTLflux_def_F=HTLflux;
Eff_def_F=Eff;
mixo_def_F=mixotrophy;
Defense_F=Bdef_all;
Defense_size_F=Bdef;
Biomass_tot_F=Btot_size.*5.68.*1e-3;
Biomass_size_F=Btot.*5.68.*1e-3;
Biomass_F=Bnew.*5.68.*1e-3;
Shannon_F=Shannon_size;
Shannon_size_F=Shannon;


load('Results_def_mumax.mat')
load('Fluxes_def_mumax.mat')
PP_def_mumax=PP;
SP_def_mumax=SP;
HTLflux_def_mumax=HTLflux;
Eff_def_mumax=Eff;
mixo_def_mumax=mixotrophy;
Defense_mumax=Bdef_all;
Defense_size_mumax=Bdef;
Biomass_size_mumax=Btot.*5.68.*1e-3;
Biomass_tot_mumax=Btot_size.*5.68.*1e-3;
Biomass_mumax=Bnew.*5.68.*1e-3;
Shannon_mumax=Shannon_size;
Shannon_size_mumax=Shannon;

load('Results_def_addcost.mat')
load('Fluxes_def_addcost.mat')
PP_def_addcost=PP;
SP_def_addcost=SP;
HTLflux_def_addcost=HTLflux;
Eff_def_addcost=Eff;
mixo_def_addcost=mixotrophy;
Defense_addcost=Bdef_all;
Defense_size_addcost=Bdef;
Biomass_size_addcost=Btot.*5.68.*1e-3;
Biomass_tot_addcost=Btot_size.*5.68.*1e-3;
Biomass_addcost=Bnew.*5.68.*1e-3;
Shannon_addcost=Shannon_size;
Shannon_size_addcost=Shannon;


    mixo_nodef_mean=zeros(2);
    mixo_Naff_mean=zeros(2,10);
    mixo_Naff_F_mean=zeros(2,10);
    mixo_F_mean=zeros(2,10);
    mixo_mumax_mean=zeros(2,10);
    mixo_addcost_mean=zeros(2,10);
    mixo_size_Naff=zeros(2,10,npmax);
    mixo_size_Naff_F=zeros(2,10,npmax);
    mixo_size_F=zeros(2,10,npmax);
    mixo_size_mumax=zeros(2,10,npmax);
    mixo_size_addcost=zeros(2,10,npmax);
    std_mixo_size_Naff=zeros(2,10,npmax);
    std_mixo_size_Naff_F=zeros(2,10,npmax);
    std_mixo_size_mumax=zeros(2,10,npmax);
    std_mixo_size_addcost=zeros(2,10,npmax);
    
    for np=1:npmax
        mixo_nodef_mean=mixo_nodef_mean+squeeze(mixo_nodef(:,np)).*squeeze(Biomass_nodef(2:3,1,1,np));
        for idef=1:ndef
           mixo_Naff_mean=mixo_Naff_mean+squeeze(mixo_def_Naff(:,1,:,np,idef)).*squeeze(Biomass_Naff(:,1,:,np,idef));
           mixo_mumax_mean=mixo_mumax_mean+squeeze(mixo_def_mumax(:,1,:,np,idef)).*squeeze(Biomass_mumax(:,1,:,np,idef));
           mixo_addcost_mean=mixo_addcost_mean+squeeze(mixo_def_addcost(:,1,:,np,idef)).*squeeze(Biomass_addcost(:,1,:,np,idef));
           mixo_Naff_F_mean=mixo_Naff_F_mean+squeeze(mixo_def_Naff_F(:,1,:,np,idef)).*squeeze(Biomass_Naff_F(:,1,:,np,idef));
           mixo_F_mean=mixo_F_mean+squeeze(mixo_def_F(:,1,:,np,idef)).*squeeze(Biomass_F(:,1,:,np,idef));
           
           mixo_size_Naff(:,:,np)=mixo_size_Naff(:,:,np)+squeeze(mixo_def_Naff(:,1,:,np,idef)).*squeeze(Biomass_Naff(:,1,:,np,idef));
           mixo_size_mumax(:,:,np)=mixo_size_mumax(:,:,np)+squeeze(mixo_def_mumax(:,1,:,np,idef)).*squeeze(Biomass_mumax(:,1,:,np,idef));
           mixo_size_addcost(:,:,np)=mixo_size_addcost(:,:,np)+squeeze(mixo_def_addcost(:,1,:,np,idef)).*squeeze(Biomass_addcost(:,1,:,np,idef));
           mixo_size_Naff_F(:,:,np)=mixo_size_Naff_F(:,:,np)+squeeze(mixo_def_Naff_F(:,1,:,np,idef)).*squeeze(Biomass_Naff_F(:,1,:,np,idef));
           mixo_size_F(:,:,np)=mixo_size_F(:,:,np)+squeeze(mixo_def_F(:,1,:,np,idef)).*squeeze(Biomass_F(:,1,:,np,idef));
       end
        mixo_size_Naff(:,:,np)=mixo_size_Naff(:,:,np)./squeeze(Biomass_size_Naff(:,1,:,np));
        mixo_size_Naff_F(:,:,np)=mixo_size_Naff_F(:,:,np)./squeeze(Biomass_size_Naff_F(:,1,:,np));
        mixo_size_F(:,:,np)=mixo_size_F(:,:,np)./squeeze(Biomass_size_F(:,1,:,np));
        mixo_size_mumax(:,:,np)=mixo_size_mumax(:,:,np)./squeeze(Biomass_size_mumax(:,1,:,np));
        mixo_size_addcost(:,:,np)=mixo_size_addcost(:,:,np)./squeeze(Biomass_size_addcost(:,1,:,np));
        
        for TS=1:2
            for tau=1:length(tau_range)
        var_mixo_size_Naff(TS,tau,np)=var(squeeze(mixo_def_Naff(TS,1,tau,np,:)),squeeze(Biomass_Naff(TS,1,tau,np,:)));
        var_mixo_size_Naff_F(TS,tau,np)=var(squeeze(mixo_def_Naff_F(TS,1,tau,np,:)),squeeze(Biomass_Naff_F(TS,1,tau,np,:)));
        var_mixo_size_mumax(TS,tau,np)=var(squeeze(mixo_def_mumax(TS,1,tau,np,:)),squeeze(Biomass_mumax(TS,1,tau,np,:)));
        var_mixo_size_addcost(TS,tau,np)=var(squeeze(mixo_def_addcost(TS,1,tau,np,:)),squeeze(Biomass_addcost(TS,1,tau,np,:)));
            end
        end
        std_mixo_size_Naff(:,:,np)=sqrt(var_mixo_size_Naff(:,:,np));
        std_mixo_size_Naff_F(:,:,np)=sqrt(var_mixo_size_Naff_F(:,:,np));
        std_mixo_size_mumax(:,:,np)=sqrt(var_mixo_size_mumax(:,:,np));
        std_mixo_size_addcost(:,:,np)=sqrt(var_mixo_size_addcost(:,:,np));

%       for idef=1:ndef
%         std_mixo_size_Naff(:,:,np)=squeeze(std_mixo_size_Naff(:,:,np))+...
%             squeeze(Biomass_Naff(:,1,:,np,idef)).*(squeeze(mixo_def_Naff(:,1,:,np,idef))-squeeze(mixo_size_Naff(:,:,np)));
%         
%         std_mixo_size_Naff_F(:,:,np)=squeeze(std_mixo_size_Naff_F(:,:,np))+...
%             squeeze(Biomass_Naff_F(:,1,:,np,idef)).*(squeeze(mixo_def_Naff_F(:,1,:,np,idef))-squeeze(mixo_size_Naff_F(:,:,np)));
%         
%          std_mixo_size_mumax(:,:,np)=squeeze(std_mixo_size_mumax(:,:,np))+...
%             squeeze(Biomass_mumax(:,1,:,np,idef)).*(squeeze(mixo_def_mumax(:,1,:,np,idef))-squeeze(mixo_size_mumax(:,:,np)));
%         
%       end
%       std_mixo_size_Naff(:,:,np)=std_mixo_size_Naff(:,:,np)./squeeze(Biomass_size_Naff(:,1,:,np));
%       std_mixo_size_Naff_F(:,:,np)=std_mixo_size_Naff_F(:,:,np)./squeeze(Biomass_size_Naff_F(:,1,:,np));
%       std_mixo_size_mumax(:,:,np)=std_mixo_size_mumax(:,:,np)./squeeze(Biomass_size_mumax(:,1,:,np));
   end
    mixo_nodef_mean=mixo_nodef_mean./squeeze(Biomass_tot_nodef(2:3));
    mixo_Naff_mean=mixo_Naff_mean./squeeze(Biomass_tot_Naff(:,1,:));
    mixo_F_mean=mixo_F_mean./squeeze(Biomass_tot_F(:,1,:));
    mixo_Naff_F_mean=mixo_Naff_F_mean./squeeze(Biomass_tot_Naff_F(:,1,:));
    mixo_mumax_mean=mixo_mumax_mean./squeeze(Biomass_tot_mumax(:,1,:));
     mixo_addcost_mean=mixo_addcost_mean./squeeze(Biomass_tot_addcost(:,1,:));
    
    
    
    suffix=['Naff    ';...
            'Naff_F  ';...
          %  'F      ';...
            'addcost '];
    varname=['Biomass_';
         'Defense_';
         'Shannon_';
         'mixo_   '];
     
     figure(9)
     for TS=1:2
        for simu=1:3
     subplot(3,2,simu*2+(TS-2))
    % plot(squeeze(Biomass_nodef(TS+1,:)),'--k')
     hold on
      variable=eval(['Biomass_size_',regexprep(suffix(simu,:),' +','')]);
        var1=squeeze(variable(TS,:,1,:));
        var2=squeeze(variable(TS,:,5,:));
%      plot(var2,'k','Linewidth',3)
%      hold on
        var3=squeeze(variable(TS,:,10,:));
        plot(1:9,var1,'k:',1:9,var2,'k-.',1:9,var3,'k-',1:9,squeeze(Biomass_nodef(TS+1,:)),'b--','Linewidth',1.5)
     %hold on
     %plot(var3,'--r') %,'Linewidth',6)
     hold on
  xticks([1:2:npmax])
      ij=0;
      for size_lab=1:2:length(V)
          ij=ij+1;
            size_labels(ij,:)=num2str(V(size_lab),'%.1e');
      end
      size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      xticklabels(size_labels)
       xlabel('Size (\mug C)')
       ylabel('Biomass (gC.m^{-3})')
       if (TS==1)
          ylim([0 0.25])
       else
          ylim([0 0.04]) 
       end
        end
     end
     legend('\tau=0.1','\tau=0.5','\tau=1','No defense')
     legend boxoff
     
     
    for fig=1:4
        figure(fig)
    for TS=1:2
        for simu=1:3
        subplot(3,2,simu*2+(TS-2))
        variable=eval([regexprep(varname(fig,:),' +',''),'size_',regexprep(suffix(simu,:),' +','')]);
        var=padarray(squeeze(variable(TS,:,:,:)).',[1,1],'post');
      pcolor(var)
       c=colorbar
       if (TS==1)
           caxis([0 1])
       else
           caxis([0 1])
       end
       colormap(jet)
       shading interp
       ylabel(c,regexprep(varname(fig,:),'_',''))
       ylabel('Size (\mug C)')
       xlabel('Trade-off')
  xticks([1:3:10])
  yticks([1:2:npmax])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      ij=0;
      for size_lab=1:2:length(V)
          ij=ij+1;
            size_labels(ij,:)=num2str(V(size_lab),'%.1e');
      end
      xticklabels(tau_labels)
      size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      yticklabels(size_labels)
      %  shading flat
        set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
        end
    end
  
    end
    
    brown=[0.4 0 0];
    figure(5)
    subplot(2,1,1)
%     shadedErrorBar([],squeeze(mixo_size_Naff(1,1,:)),squeeze(std_mixo_size_Naff(1,10,:)),{'color',brown,'Linewidth',1.5},0.5)
%     hold on
    shadedErrorBar([],squeeze(mixo_size_Naff(1,10,:)),squeeze(std_mixo_size_Naff(1,10,:)),{'r','Linewidth',1.5},0.5)
    hold on
    shadedErrorBar([],squeeze(mixo_size_Naff_F(1,10,:)),squeeze(std_mixo_size_Naff_F(1,10,:)),{'g','Linewidth',1.5},0.5)
    hold on
    shadedErrorBar([],squeeze(mixo_size_addcost(1,10,:)),squeeze(std_mixo_size_addcost(1,10,:)),{'color',brown,'Linewidth',1.5},0.5)
    hold on
     plot(mixo_nodef(1,:),'b--','Linewidth',1.5)
    hold on
    ylim([0 1])
  %  plot(squeeze(mixo_size_Naff(1,10,:)),'r')
%     hold on
%     plot(squeeze(mixo_size_Naff(1,1,:)),'b')
%     hold on
%     plot(squeeze(mixo_size_mumax(1,10,:)),'b')
%     hold on
%     plot(squeeze(mixo_size_Naff_F(1,10,:)),'g')
     %size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      size_labels=['10^{-7}';'10^{-6}';'10^{-5}';'10^{-4}';'10^{-3}';'10^{-2}';'10^{-1}';'10^{0 }';'10^{1 }'];
      xticklabels(size_labels)
      xlabel('Size (\mug C)')
      ylabel('Trophic Strategy')
     legend('Cost Naff','Cost Naff+Faff','Add. Cost','No defense','Location','Best')
 

       set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
     
    subplot(2,1,2)
   % shadedErrorBar([],squeeze(mixo_size_Naff(2,1,:)),squeeze(std_mixo_size_Naff(2,1,:)),{'color',brown,'linewidth',1.5},0.5)
   % hold on
    shadedErrorBar([],squeeze(mixo_size_Naff(2,10,:)),squeeze(std_mixo_size_Naff(2,10,:)),{'r','linewidth',1.5},0.5)
    hold on
    shadedErrorBar([],squeeze(mixo_size_Naff_F(2,10,:)),squeeze(std_mixo_size_Naff_F(2,10,:)),{'g','Linewidth',1.5},0.5)
    hold on
    shadedErrorBar([],squeeze(mixo_size_addcost(2,10,:)),squeeze(std_mixo_size_addcost(2,10,:)),{'color',brown,'Linewidth',1.5},0.5)
    hold on
      plot(squeeze(mixo_nodef(2,:)),'b--','Linewidth',1.5)
    hold on
    ylim([0 1])
%     plot(squeeze(mixo_size_Naff(2,10,:)),'r')
%     hold on
% %     plot(squeeze(mixo_size_Naff(2,1,:)),'b')
% %     hold on
%     plot(squeeze(mixo_size_mumax(2,10,:)),'b')
%     hold on
%     plot(squeeze(mixo_size_Naff_F(2,10,:)),'g')
    size_labels=['10^{-7}';'10^{-6}';'10^{-5}';'10^{-4}';'10^{-3}';'10^{-2}';'10^{-1}';'10^{0 }';'10^{1 }'];
      xticklabels(size_labels)
      xlabel('Size (\mug C)')
      ylabel('Trophic Strategy')
    %  legend('Nodefense','Cost on nut. affinity','Cost on \mumax','Cost on nut. affinity + phagotrophy',...
    %        'Location','Best')
     set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
    

figure(6)
     subplot(2,1,1)
    plot(squeeze(Biomass_nodef(2,:)),'k','LineWidth',14)
    ylim([0 0.25])
    set(gca, 'XDir','reverse')
     lw = 10;      % LineWidth
     set(gca,'LineWidth',lw)
     subplot(2,1,2)
    plot(squeeze(Biomass_nodef(3,:)),'k','LineWidth',14)
    ylim([0 0.015])
    set(gca, 'XDir','reverse')
     lw = 10;      % LineWidth
     set(gca,'LineWidth',lw)
     
    
    figure(7)
    TS=1;
    tau_plot=[1 10];
    for itau=1:2
    for simu=1:3
        tau=tau_plot(itau);
        subplot(3,2,simu*2+(itau-2))
        variable=eval(['Biomass_',regexprep(suffix(simu,:),' +','')]);
       var=padarray(squeeze(variable(TS,:,tau,:,1:10)).',[1,1],'post');
      pcolor(var)
       c=colorbar
         caxis([0 1])
          colormap(jet)
       shading interp
       ylabel(c,regexprep(varname(fig,:),'_',''))
       ylabel('Size (\mug C)')
       xlabel('Trade-off')
  xticks([1:3:10])
  yticks([1:2:npmax])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      ij=0;
      for size_lab=1:2:length(V)
          ij=ij+1;
            size_labels(ij,:)=num2str(V(size_lab),'%.1e');
      end
      xticklabels(tau_labels)
      size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      yticklabels(size_labels)
      %  shading flat
        set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
    end
    end
    

    
    figure(7)
    TS=1;
    tau_plot=[1 10];
    for itau=1:2
    for simu=1:3
        tau=tau_plot(itau);
        subplot(3,2,simu*2+(itau-2))
        variable=eval(['Biomass_',regexprep(suffix(simu,:),' +','')]);
       var=padarray(squeeze(variable(TS,:,tau,:,1:10)).',[1,1],'post');
      pcolor(var)
       c=colorbar
       caxis([0 0.05])
        colormap(jet)
      shading interp
       ylabel(c,'Biomass (gC.m^{-3})')
       xlabel('Size (\mug C)')
       ylabel('Defense')
       
  yticks([1:3:10])
  xticks([1:2:npmax])
     
      def_labels=['0.1';'0.4';'0.7';'  1'];
      size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      xticklabels(size_labels)
       yticklabels(def_labels)
        shading interp
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
    end
    end
    
    figure(8)
    TS=2;
    tau_plot=[1 10];
    for itau=1:2
    for simu=1:3
        tau=tau_plot(itau);
        subplot(3,2,simu*2+(itau-2))
        variable=eval(['Biomass_',regexprep(suffix(simu,:),' +','')]);
       var=padarray(squeeze(variable(TS,:,tau,:,1:10)).',[1,1],'post');
      pcolor(var)
       c=colorbar
      caxis([0 0.005])
        colormap(jet)
      shading interp
       ylabel(c,'Biomass (gC.m^{-3})')
       xlabel('Size (\mug C)')
       ylabel('Defense')
       
  yticks([1:3:10])
  xticks([1:2:npmax])
     
      def_labels=['0.1';'0.4';'0.7';'  1'];
      size_labels=['10^{-7}';'10^{-5}';'10^{-3}';'10^{-1}';'10^{1 }'];
      xticklabels(size_labels)
       yticklabels(def_labels)
        shading interp
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
    end
    end
    