clear all 
close all 
clf
clc

%Figures Settings
  width = 10;     % Width in inches
  height =10;    % Height in inches
  alw = 4;    % AxesLineWidth
  fsz = 18;      % Fontsize
  lw = 4;      % LineWidth
  
  V_low = -7;
 V_high = 1;
 V_div = 9 ; 
 V = logspace(V_low,V_high,V_div); % size range 
%V=[1e-4 4e-1 1e4];
%V=[1e-2 1e-1];
npmax=length(V);
xi_pops=[0.1:0.1:1];
ndef=length(xi_pops);

 N0_range=[168 168 14]; % Nitrogen concentration in aphotic layer 100    
 tau_range=[0.1:0.1:1];




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Figures model mixotrophy size-based
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
load('Results_nodef.mat')
load('Fluxes_nodef.mat')
Shannon_nodef=squeeze(Shannon(2:3));
PP_nodef=squeeze(PP(2:3));
SP_nodef=squeeze(SP(2:3));
HTLflux_nodef=squeeze(HTLflux(2:3));
Eff_nodef=squeeze(Eff(2:3));
mixo_nodef=squeeze(mixotrophy(2:3,:));

load('Results_def_Naff.mat')
load('Fluxes_def_Naff.mat')
PP_def_Naff=PP;
SP_def_Naff=SP;
HTLflux_def_Naff=HTLflux;
Eff_def_Naff=Eff;
mixo_def_Naff=mixotrophy;
Defense_Naff=Bdef_all;
Defense_size_Naff=Bdef;
Biomass_tot_Naff=Btot_size.*5.68.*1e-3;
Biomass_size_Naff=Btot.*5.68.*1e-3;
Biomass_Naff=Bnew.*5.68.*1e-3;
Shannon_Naff=Shannon_size;
Shannon_size_Naff=Shannon;


load('Results_def_Naff_F.mat')
load('Fluxes_def_Naff_F.mat')
PP_def_Naff_F=PP;
SP_def_Naff_F=SP;
HTLflux_def_Naff_F=HTLflux;
Eff_def_Naff_F=Eff;
mixo_def_Naff_F=mixotrophy;
Defense_Naff_F=Bdef_all;
Defense_size_Naff_F=Bdef;
Biomass_tot_Naff_F=Btot_size.*5.68.*1e-3;
Biomass_size_Naff_F=Btot.*5.68.*1e-3;
Biomass_Naff_F=Bnew.*5.68.*1e-3;
Shannon_Naff_F=Shannon_size;
Shannon_size_Naff_F=Shannon;

load('Results_def_F.mat')
load('Fluxes_def_F.mat')
PP_def_F=PP;
SP_def_F=SP;
HTLflux_def_F=HTLflux;
Eff_def_F=Eff;
mixo_def_F=mixotrophy;
Defense_F=Bdef_all;
Defense_size_F=Bdef;
Biomass_tot_F=Btot_size.*5.68.*1e-3;
Biomass_size_F=Btot.*5.68.*1e-3;
Biomass_F=Bnew.*5.68.*1e-3;
Shannon_F=Shannon_size;
Shannon_size_F=Shannon;


load('Results_def_mumax.mat')
load('Fluxes_def_mumax.mat')
PP_def_mumax=PP;
SP_def_mumax=SP;
HTLflux_def_mumax=HTLflux;
Eff_def_mumax=Eff;
mixo_def_mumax=mixotrophy;
Defense_mumax=Bdef_all;
Defense_size_mumax=Bdef;
Biomass_size_mumax=Btot.*5.68.*1e-3;
Biomass_tot_mumax=Btot_size.*5.68.*1e-3;
Biomass_mumax=Bnew.*5.68.*1e-3;
Shannon_mumax=Shannon_size;
Shannon_size_mumax=Shannon;

load('Results_def_addcost.mat')
load('Fluxes_def_addcost.mat')
PP_def_addcost=PP;
SP_def_addcost=SP;
HTLflux_def_addcost=HTLflux;
Eff_def_addcost=Eff;
mixo_def_addcost=mixotrophy;
Defense_addcost=Bdef_all;
Defense_size_addcost=Bdef;
Biomass_size_addcost=Btot.*5.68.*1e-3;
Biomass_tot_addcost=Btot_size.*5.68.*1e-3;
Biomass_addcost=Bnew.*5.68.*1e-3;
Shannon_addcost=Shannon_size;
Shannon_size_addcost=Shannon;



    mixo_nodef_mean=zeros(2);
    mixo_Naff_mean=zeros(2,10);
    mixo_Naff_F_mean=zeros(2,10);
    mixo_F_mean=zeros(2,10);
    mixo_mumax_mean=zeros(2,10);
    mixo_addcost_mean=zeros(2,10);
    mixo_size_Naff_mean=zeros(2,10,npmax);
    mixo_size_Naff_F_mean=zeros(2,10,npmax);
    mixo_size_F_mean=zeros(2,10,npmax);
    mixo_size_mumax_mean=zeros(2,10,npmax);
    mixo_size_addcost_mean=zeros(2,10,npmax);
    for np=1:npmax
        mixo_nodef_mean=mixo_nodef_mean+squeeze(mixo_nodef(:,np)).*squeeze(Biomass_nodef(2:3,1,1,np));
       for idef=1:ndef
           mixo_Naff_mean=mixo_Naff_mean+squeeze(mixo_def_Naff(:,1,:,np,idef)).*squeeze(Biomass_Naff(:,1,:,np,idef));
           mixo_mumax_mean=mixo_mumax_mean+squeeze(mixo_def_mumax(:,1,:,np,idef)).*squeeze(Biomass_mumax(:,1,:,np,idef));
           mixo_addcost_mean=mixo_addcost_mean+squeeze(mixo_def_addcost(:,1,:,np,idef)).*squeeze(Biomass_addcost(:,1,:,np,idef));
           mixo_Naff_F_mean=mixo_Naff_F_mean+squeeze(mixo_def_Naff_F(:,1,:,np,idef)).*squeeze(Biomass_Naff_F(:,1,:,np,idef));
           mixo_F_mean=mixo_F_mean+squeeze(mixo_def_F(:,1,:,np,idef)).*squeeze(Biomass_F(:,1,:,np,idef));
           
           mixo_size_Naff_mean(:,:,np)=mixo_size_Naff_mean(:,:,np)+squeeze(mixo_def_Naff(:,1,:,np,idef)).*squeeze(Biomass_Naff(:,1,:,np,idef));
           mixo_size_mumax_mean(:,:,np)=mixo_size_mumax_mean(:,:,np)+squeeze(mixo_def_mumax(:,1,:,np,idef)).*squeeze(Biomass_mumax(:,1,:,np,idef));
           mixo_size_addcost_mean(:,:,np)=mixo_size_addcost_mean(:,:,np)+squeeze(mixo_def_addcost(:,1,:,np,idef)).*squeeze(Biomass_addcost(:,1,:,np,idef));
           mixo_size_Naff_F_mean(:,:,np)=mixo_size_Naff_F_mean(:,:,np)+squeeze(mixo_def_Naff_F(:,1,:,np,idef)).*squeeze(Biomass_Naff_F(:,1,:,np,idef));
           mixo_size_F_mean(:,:,np)=mixo_size_F_mean(:,:,np)+squeeze(mixo_def_F(:,1,:,np,idef)).*squeeze(Biomass_F(:,1,:,np,idef));
       end
        mixo_size_Naff_mean(:,:,np)=mixo_size_Naff_mean(:,:,np)./squeeze(Biomass_size_Naff(:,1,:,np));
        mixo_size_Naff_F_mean(:,:,np)=mixo_size_Naff_F_mean(:,:,np)./squeeze(Biomass_size_Naff_F(:,1,:,np));
        mixo_size_F_mean(:,:,np)=mixo_size_F_mean(:,:,np)./squeeze(Biomass_size_F(:,1,:,np));
        mixo_size_mumax_mean(:,:,np)=mixo_size_mumax_mean(:,:,np)./squeeze(Biomass_size_mumax(:,1,:,np));
         mixo_size_addcost_mean(:,:,np)=mixo_size_addcost_mean(:,:,np)./squeeze(Biomass_size_addcost(:,1,:,np));
    end
    mixo_nodef_mean=mixo_nodef_mean./squeeze(Biomass_tot_nodef(2:3));
    mixo_Naff_mean=mixo_Naff_mean./squeeze(Biomass_tot_Naff(:,1,:));
    mixo_F_mean=mixo_F_mean./squeeze(Biomass_tot_F(:,1,:));
    mixo_Naff_F_mean=mixo_Naff_F_mean./squeeze(Biomass_tot_Naff_F(:,1,:));
    mixo_mumax_mean=mixo_mumax_mean./squeeze(Biomass_tot_mumax(:,1,:));
    mixo_addcost_mean=mixo_addcost_mean./squeeze(Biomass_tot_addcost(:,1,:));
   
for TS=1:2
     figure(1)
      subplot(1,2,TS)
     plot(tau_range,squeeze(Biomass_tot_Naff(TS,1,:)),'r')
     hold on
      plot(tau_range,squeeze(Biomass_tot_mumax(TS,1,:)),'c')
     hold on
      plot(tau_range,squeeze(Biomass_tot_Naff_F(TS,1,:)),'m')
     hold on
%       plot(tau_range,squeeze(Biomass_tot_F(TS,1,:)),'b')
%      hold on
     plot(tau_range,repmat(Biomass_tot_nodef(TS+1,1),size(tau_range)),'--g');
     hold on 
     ylabel('Plankton Biomass (gC.m^{-3})')
   %  ylim([0 1])
    % legend('Cost on nut. affinity','Cost on \mumax','Cost on nut. affinity + phagotrophy',...
    %        'Cost on phagotrophy','NoDefense','Location','Best')
     set(gca, 'XTickLabel', get(gca,'XTick'))
       xlabel('Trade-off Exponent')
      xticks([0.1:0.3:1])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      xticklabels(tau_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')

brown=[0.4 0 0];
      figure(2)
      subplot(1,2,TS)
     plot(tau_range,squeeze(Shannon_Naff(TS,1,:)),'r','Linewidth',1.5)
     hold on
      plot(tau_range,squeeze(Shannon_addcost(TS,1,:)),'color',brown,'Linewidth',1.5)
     hold on
      plot(tau_range,squeeze(Shannon_Naff_F(TS,1,:)),'g','Linewidth',1.5)
     hold on
     
%       plot(tau_range,squeeze(Shannon_F(TS,1,:)),'b')
%      hold on
     plot(tau_range,repmat(Shannon_nodef(TS,1),size(tau_range)),'k--','linewidth',1.5');
     hold on 
     ylabel('Size Diversity Shannon Index')
     ylim([0.5 1])
     set(gca, 'XTickLabel', get(gca,'XTick'))
       xlabel('\tau')
       xlim([0.1 1])
      xticks([0.1:0.3:1])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      xticklabels(tau_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')

      figure(3)
      subplot(1,2,TS)
     plot(tau_range,squeeze(Defense_Naff(TS,1,:)),'r')
     hold on
      plot(tau_range,squeeze(Defense_mumax(TS,1,:)),'c')
     hold on
      plot(tau_range,squeeze(Defense_Naff_F(TS,1,:)),'m')
     hold on
%       plot(tau_range,squeeze(Defense_F(TS,1,:)),'b')
%      hold on
     ylabel('Mean Defense Strategy')
     ylim([0 1])
   %  legend('Cost on nut. affinity','Cost on \mumax','Cost on nut. affinity + phagotrophy',...
   %         'Cost on phagotrophy','Location','Best')
     set(gca, 'XTickLabel', get(gca,'XTick'))
       xlabel('Trade-off Exponent')
      xticks([0.1:0.3:1])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      xticklabels(tau_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')

    figure(4)
      subplot(1,2,TS)
     plot(tau_range,squeeze(mixo_Naff_mean(TS,:)),'r')
     hold on
      plot(tau_range,squeeze(mixo_mumax_mean(TS,:)),'c')
     hold on
      plot(tau_range,squeeze(mixo_Naff_F_mean(TS,:)),'m')
     hold on
%       plot(tau_range,squeeze(mixo_F_mean(TS,:)),'b')
%      hold on
      plot(tau_range,repmat(mixo_nodef_mean(TS),size(tau_range)),'--g');
     hold on 
     ylabel('Mixotrophy')
     ylim([0 1])
  %   legend('Cost on nut. affinity','Cost on \mumax','Cost on nut. affinity + phagotrophy',...
  %          'Cost on phagotrophy','Location','Best')
     set(gca, 'XTickLabel', get(gca,'XTick'))
       xlabel('Trade-off Exponent')
        legend('Cost on nut. affinity','Cost on \mumax','Cost on nut. affinity + phagotrophy',...
            'NoDefense','Location','Best')
      xticks([0.1:0.3:1])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      xticklabels(tau_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')


      figure(5)
      subplot(1,2,TS)
     plot(tau_range,squeeze(PP_def_Naff(TS,1,:)),'r')
     hold on
      plot(tau_range,squeeze(PP_def_mumax(TS,1,:)),'c')
     hold on
      plot(tau_range,squeeze(PP_def_Naff_F(TS,1,:)),'m')
     hold on
%       plot(tau_range,squeeze(PP_def_F(TS,1,:)),'b')
%      hold on
     plot(tau_range,repmat(PP_nodef(TS),size(tau_range)),'--g');
     hold on 
     ylabel('PP')
   %  ylim([0 1])
 %    legend('Cost on nut. affinity','Cost on \mumax','Cost on nut. affinity + phagotrophy',...
 %           'Cost on phagotrophy','NoDefense','Location','Best')
     set(gca, 'XTickLabel', get(gca,'XTick'))
       xlabel('Trade-off')
      xticks([0.1:0.3:1])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      xticklabels(tau_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')


      figure(6)
      subplot(1,2,TS)
     plot(tau_range,squeeze(SP_def_Naff(TS,1,:)),'r')
     hold on
      plot(tau_range,squeeze(SP_def_mumax(TS,1,:)),'c')
     hold on
      plot(tau_range,squeeze(SP_def_Naff_F(TS,1,:)),'m')
     hold on
%       plot(tau_range,squeeze(SP_def_F(TS,1,:)),'b')
%      hold on
     plot(tau_range,repmat(SP_nodef(TS),size(tau_range)),'--g');
     hold on 
     ylabel('SP')
   %  ylim([0 1])
 %    legend('Cost on nut. affinity','Cost on \mumax','Cost on nut. affinity + phagotrophy',...
 %           'Cost on phagotrophy','NoDefense','Location','Best')
     set(gca, 'XTickLabel', get(gca,'XTick'))
       xlabel('Trade-off')
      xticks([0.1:0.3:1])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      xticklabels(tau_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')

     figure(7)
      subplot(1,2,TS)
     plot(tau_range,squeeze(Eff_def_Naff(TS,1,:)),'r')
     hold on
      plot(tau_range,squeeze(Eff_def_mumax(TS,1,:)),'c')
     hold on
      plot(tau_range,squeeze(Eff_def_Naff_F(TS,1,:)),'m')
     hold on
%       plot(tau_range,squeeze(Eff_def_F(TS,1,:)),'b')
%      hold on
     plot(tau_range,repmat(Eff_nodef(TS),size(tau_range)),'--g');
     hold on 
     ylabel('Eff')
  %   ylim([0 1])
 %    legend('Cost on nut. affinity','Cost on \mumax','Cost on nut. affinity + phagotrophy',...
 %           'Cost on phagotrophy','NoDefense','Location','Best')
     set(gca, 'XTickLabel', get(gca,'XTick'))
       xlabel('Trade-off')
      xticks([0.1:0.3:1])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      xticklabels(tau_labels)
set(gca,'FontSize',fsz)
set(0,'defaultAxesFontName', 'Arial')
set(0,'defaultTextFontName', 'Arial')
    
end %eutrophic vs. oligotrophic (subplots)
    