clear all
clc
clf
close all

global r N0 P0 xi xi_range xi_pops ndef opt_flag competition tau seasonal
global phiN phiF phiL cL cN cF aF aN aL R0 m0 cCN mumax mHT mHT_quad light 
global V npmax m1 remin_rate xi_old dil_rate
 

% Parameters values : 
cCN=5.68;
phiN=0.2;
phiF=0.3;
phiL=0.5;
cL=0.01;
cN=0.00016;
cF=0.04;
aL=1;
aF=0.06;
aN=0.12; %code Subhendu : 0.12 affinity for nutrients
%Parameters from Chakraborty et al., 2017
R0=0.04; %d-1 
m0=0.0; %linear constant mortality
m1=0.01; %quadratic mortality
%To be define
mumax=1; %d-1
mHT=0.06;
mHT_quad=0.001;
% beta=1000;
% sigma=1.5;
remin_rate=0.8;

 V_low = -7;
 V_high = 1;
 V_div = 9 ; 
 V = logspace(V_low,V_high,V_div); % size range 
%V=[1e-4 4e-1 1e4];
%V=[1e-2 1e-1];
npmax=length(V);


opt_flag=0;
competition=1;
seasonal=1;

%Figures Settings
  width = 10;     % Width in inches
  height =10;    % Height in inches
  alw = 2;    % AxesLineWidth
  fsz = 2;      % Fontsize
  lw = 2;      % LineWidth
  
orange=[1 0.6 0];
% Parameters values :
%r_range=[0.01];
%N0_range=[140];
%r_range=[0.1 0.25 0.5]; %0.05; % Dissolution rate 
r_range=[0.1];
%N0_range=[140 540 1120]; % Nitrogen concentration in aphotic layer 100
% N0_range=[168 14];
% light_range=[50 50];
N0_range=[140]; %seasonally varying light case
%light_range=[1 50 50];
%alpha=1; % Maximal Uptake rate of preys  
%alpha=1e-2; %1e-2; % Maximal Uptake rate of preys 
%beta=1e-3; %1e-3; % Predator grazing rate 
%beta_range=[0.01;0.02;0.05;0.1];
%beta_range=1e-3; %;0.02;0.05;0.1];
%sigma=0.01; %0.5; %higher trophic levels predation rate 

%Defense investment (0 : maximal investment)
if (opt_flag)
    range_S=[0.1:0.1:1];
    %range_S=1;
    ndef=1;
elseif (competition)
xi_pops=[0:0.1:0.9]; % different phytoplankton competing 
%xi_pops=[0.2;0.4;0.6;0.8];
ndef=length(xi_pops);
else
%xi_range=[0.1:0.1:1]; % only one phytoplankton - test for different defense levels     
xi_range=0;
ndef=1;
end
%Shape trade-offs
%tau_range=[0.1];
%tau_range=[0.1:0.1:1];
tau_range=[1];

nt=100;

for TS=1:length(N0_range) %Trophic status
    for R=1:length(r_range)
    r=r_range(R)
    N0=N0_range(TS)
    P0=0.001*N0;
    if (seasonal)
        temps=1:365;
        radsw_tot=0+20*(1-cos(2*pi*temps/365));
        light=repmat(radsw_tot.',10,1);    
    load('PAR_MLD_year_stratif.mat')
    
    light=smooth(PAR_stratif,100);
    dil_rate=r.*(1-((light-min(light))./(max(light)-min(light))));
    dil_rate(120:250)=0.001;
    light=repmat(light,10,1);
    dil_rate=repmat(dil_rate,10,1);
    %dil_rate(1:size(dil_rate))=r;
    %light=smooth(light,100);
   
    else
    light=light_range(TS)
    end
    %Conditions initiales
if (competition)
%B0(1,1)=N0/2;
%B0(1,2:ndef)=0;
%B0(2:npmax+1,1:ndef)=N0/(2*ndef*npmax);
B0(1,1)=N0;
B0(1,2:ndef)=0;
B0(2:npmax+1,1:ndef)=0.1./ndef;
Bini=reshape(B0,[(npmax+1)*ndef 1]);
else
B0(1,1)=N0;
B0(2:npmax+1,1)=0.1;
%B0(1:npmax+1,1)=0.1;
%B0(2:npmax+1,1)=N0/(2*npmax);
end
   if (~competition && ~opt_flag) % different simulations with varying xi (one phyto)
      for n=1:length(xi_range)
            for ii=1:npmax
                  xi(ii,1)=xi_range(n);
              end
          for i=1:length(tau_range)
            
         tau=tau_range(i);
         %xi=xi_range(n);
      %      options=optimoptions('fsolve','Display','Iter');
       %  B =fsolve(@ode_test_defense_mixo,B0,options);
         %[dBdt mixo] = ode_test_defense_mixo(B);
          odefun = @(t,B) ode_test_defense_mixo_with_light_cost_Naffinity(B);
          efun = @(t,B) odeevent2(B);
           options=odeset('RelTol',1e-3,'AbsTol',1e-3,'Events',efun,'NonNegative',1);         
         [t,Bode]=ode45(odefun,[0:5*365],B0,options);
        
         if (seasonal)
        for it=1:size(Bode,1)
            B_it(1:npmax+1,1)=squeeze(Bode(it,:));
  [dBdt,PP_tot(it),SP_tot(it),mort_HTL(it),TTE(it),mixo]=ode_test_defense_mixo_with_light_cost_Naffinity(it,B_it);
        mixotrophy_nodef(it,:,:)=mixo;
        end
        PP_nodef=PP_tot;
        SP_nodef=SP_tot;
        mortHTL_nodef=mort_HTL;
        TTE_nodef=TTE;
            save('Fluxes_nodef_seasonal.mat','mixotrophy_nodef','PP_nodef','SP_nodef','mortHTL_nodef','TTE_nodef')
            save('Biomass_nodef_seasonal.mat','Bode')   
         else
          Bend=squeeze(Bode(size(Bode,1),:).');
         %[t,B]=ode23(@ode_test_defense,[0:nt],B0);
         %B_xi(n,:,:)=B; %ode45
         B_xi(TS,R,i,n,:)=Bend;
%         mixo_xi(TS,R,i,n,:,:)=mixo;
            [dBdt,PP_tot,SP_tot,mort_HTL,TTE,mixo]=ode_test_defense_mixo_with_light_cost_Naffinity(Bend);          
           PP(TS)=PP_tot;
           SP(TS)=SP_tot;
           HTLflux(TS)=mort_HTL;
           Eff(TS)=TTE;
           mixotrophy(TS,:)=mixo;
            save('Fluxes_nodefense.mat','mixotrophy','Eff','SP','PP','HTLflux')
         end
%           figure(44+TS)
%           subplot(2,1,1)
%           plot(Bode(:,1))
%           subplot(2,1,2)
%              for np=2:npmax+1
%                 plot(Bode(:,np))
%                 hold on
%              end
%        
          end %shape trade-off
      end %defense
%   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      %Plot Biomass of each size class
%       for fig=1:length(xi_range)
%       figure(fig)
%       subplot(length(r_range),length(N0_range),R*length(N0_range)+(TS-length(N0_range)))
%       Biomass=squeeze(B_xi(TS,R,:,fig,2:npmax+1).*cCN.*1e-3);      
% %       tau_2d=repmat(tau_range.',[1 length(V)]);
% %       V_2d=repmat(V,[length(tau_range) 1]);
% %       pcolor(tau_2d,V_2d,Biomass)
%      padded=padarray(Biomass.',[1,1],'post');
%      pcolor(padded)
% %       Biomass=fliplr(Biomass)
% %       imagesc(Biomass.')
%       c=colorbar
%     %  caxis([0 N0])
%       set(gca,'Yscale','log')
%       ylabel(c,'Biomass (mgC.m^{-3})')
%       ylabel('Size (\mug C)')
%       set(gca, 'XTickLabel', get(gca,'XTick'))
%       xlabel('Trade-off')
%      xticks([0.1:0.3:1])
%      ii=0;
%      for tau_lab=1:3:length(tau_range)
%         ii=ii+1;
%         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
%      end
%      xticklabels(tau_labels)
%      shading flat
%       colormap(jet)
%      title(strcat(['r=',num2str(r),'-N=',num2str(N0)]))
%       set(gca,'FontSize',fsz)
%       set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
%    
%      if (R==length(r_range) & TS==length(N0_range)) 
%   pos = get(gcf, 'Position');
%   set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
%   set(gcf,'InvertHardcopy','on');
%   set(gcf,'PaperUnits', 'inches');
%   papersize = get(gcf, 'PaperSize');
%   left = (papersize(1)- width)/2;
%   bottom = (papersize(2)- height)/2;
%   myfiguresize = [left, bottom, width, height];
%   set(gcf,'PaperPosition', myfiguresize);
%   saveas(gcf,strcat(['Biomass_S=',num2str(fig),'.jpg']),'jpg');
%      end

if (~ seasonal)
     Biomass=squeeze(B_xi(TS,R,1,1,2:npmax+1).*cCN.*1e-3); 
     
%Compute Shannon Index 
Bshannon=Biomass;
          Bshannon(Bshannon<0.001)=0.001;
          Shannon(TS,R,1)=0;
          Btot=sum(Bshannon);
          for np=1:npmax
          prop_size(np)=Bshannon(np)./Btot;
          Shannon(TS,R,1)=Shannon(TS,R,1)+prop_size(np).*log(prop_size(np));
          end
           Shannon(TS,R,1)=-Shannon(TS,R,1)./log(npmax);
           Biomass_tot_nodef(TS,R,1)=sum(Biomass);
           Biomass_nodef(TS,R,1,:)=Biomass;
     
     figure(50)
     
     subplot(1,length(N0_range),TS)
    semilogx(V,squeeze(Biomass(:)),'ko')
    xlim([1e-8 1e2])
    hold on
   % xticks([1:1:length(V)])
    % set(gca, 'XTickLabel',V)
xlabel('Size (\mugC)')
ylabel('Biomass (\mugN.L^{-1})')
set(gca,'FontSize',fsz)
      set(0,'defaultAxesFontName', 'Arial')
    set(0,'defaultTextFontName', 'Arial')
   
     if (R==length(r_range) & TS==length(N0_range)) 
  pos = get(gcf, 'Position');
  set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
  set(gcf,'InvertHardcopy','on');
  set(gcf,'PaperUnits', 'inches');
  papersize = get(gcf, 'PaperSize');
  left = (papersize(1)- width)/2;
  bottom = (papersize(2)- height)/2;
  myfiguresize = [left, bottom, width, height];
  set(gcf,'PaperPosition', myfiguresize);
  saveas(gcf,'Biomass_size_class_nodef.jpg','jpg');
     end
     
end %seasonal cycle or not  
%    % Plot mixotrophic strategies
%       figure(length(xi_range)+fig)
%         subplot(length(r_range),length(N0_range),R*length(N0_range)+(TS-length(N0_range)))
%       mixotrophy=squeeze(mixo_xi(TS,R,:,fig,:,:));  
%         mixotrophy(Biomass<10)=NaN;
%       tau_2d=repmat(tau_range.',[1 length(V)]);
%       V_2d=repmat(V,[length(tau_range) 1]);
%       pcolor(tau_2d,V_2d,mixotrophy)
%       c=colorbar
%       caxis([0 1])
%       set(gca,'Yscale','log')
%       ylabel(c,'Mixotrophy')
%       ylabel('Size (\mug C)')
%       set(gca, 'XTickLabel', get(gca,'XTick'))
%       xlabel('Trade-off')
%      xticks([0.1:0.3:1])
%      ii=0;
%      for tau_lab=1:3:length(tau_range)
%         ii=ii+1;
%         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
%      end
%      xticklabels(tau_labels)
%      shading flat
%       colormap(jet)
%      title(strcat(['r=',num2str(r),'-N=',num2str(N0)]))
%       set(gca,'FontSize',fsz)
%       set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
%    
%      if (R==length(r_range) & TS==length(N0_range)) 
%   pos = get(gcf, 'Position');
%   set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
%   set(gcf,'InvertHardcopy','on');
%   set(gcf,'PaperUnits', 'inches');
%   papersize = get(gcf, 'PaperSize');
%   left = (papersize(1)- width)/2;
%   bottom = (papersize(2)- height)/2;
%   myfiguresize = [left, bottom, width, height];
%   set(gcf,'PaperPosition', myfiguresize);
%   saveas(gcf,strcat(['Mixotrophy_S=',num2str(fig),'.jpg']),'jpg');
%      end    
%     end%nb figures
%   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    elseif (competition)
        for ii=1:npmax
            for jj=1:ndef
        xi(ii,jj)=xi_pops(jj);
            end
        end %size classe 
          clear B_tau
        for i=1:length(tau_range)
         %   for i=7:10
          tau=tau_range(i);
          %options=optimset('LargeScale','off','TolFun',5,'MaxIter',1000000000,'MaxFunEvals',100000000);
         % options=optimset(
         % options=odeset('RelTol',1e-8,'AbsTol',1e-8);
         odefun = @(t,B) ode_test_defense_mixo_with_light_cost_Naffinity(B,t);
            efun = @(t,B) odeevent2(B);
      % options=odeset('RelTol',1e-3,'AbsTol',1e-3,'Events',efun,'NonNegative',1); 
           [t,B]=ode45(odefun,[0:5*365],Bini);  
           if (seasonal)
               for it=1:size(B,1)
                   Btime(it,:,:)=reshape(squeeze(B(it,:)),[npmax+1 ndef]);        
  [dBdt,PP_tot(it),SP_tot(it),mort_HTL(it),TTE(it),mixo]=ode_test_defense_mixo_with_light_cost_Naffinity(B(it,:),it);
        mixotrophy_def(it,:,:)=mixo;
               end
             %  Btime=reshape(B,[size(B,1) npmax+1 ndef])
               save('Biomass_def_seasonal_addcost.mat','Btime')  
        PP_def=PP_tot;
        SP_def=SP_tot;
        mortHTL_def=mort_HTL;
        TTE_def=TTE;
            save('Fluxes_def_seasonal_addcost.mat','mixotrophy_def','PP_def','SP_def','mortHTL_def','TTE_def')
           else
            Beq=squeeze(B(size(B,1),:));
              if (size(B,1)<1001)
                  for t=size(B,1)+1:1001
               B(t,1:size(B,2))=Beq(:);
                  end
              end
                B_tau(i,:,:)=B;
                
            [dBdt,PP_tot,SP_tot,mort_HTL,TTE,mixo]=ode_test_defense_mixo_with_light_cost_Naffinity(Beq);     
           Beq=reshape(Beq,[npmax+1 ndef]); 
           
           PP(TS,R,i)=PP_tot;
           SP(TS,R,i)=SP_tot;
           HTLflux(TS,R,i)=mort_HTL;
           Eff(TS,R,i)=TTE;
           mixotrophy(TS,R,i,:,:)=mixo;
           
            
          Bnew(TS,R,i,:,:)=squeeze(Beq(2:npmax+1,:)); 
          %Biomass par classe de taille
          Btot(TS,R,i,:)=sum(squeeze(Bnew(TS,R,i,:,:)),2);
          Btot_size(TS,R,i)=sum(squeeze(Btot(TS,R,i,:)));
          Bshannon_size(TS,R,i,:)=Btot(TS,R,i,:);
          Bshannon_size(Bshannon_size<0.001)=0.001;
          Bshannon(TS,R,i,:,:)=Bnew(TS,R,i,:,:);
          Bshannon(Bshannon<0.001)=0.001;
          
          Bdef_all(TS,R,i)=0;
          Shannon_size(TS,R,i)=0;
          for np=1:npmax
          prop_size(np)=Bshannon_size(TS,R,i,np)./Btot_size(TS,R,i);
          Shannon_size(TS,R,i)=Shannon_size(TS,R,i)+prop_size(np).*log(prop_size(np));
          Bdef(TS,R,i,np)=0;
          Shannon(TS,R,i,np)=0;
          for idef=1:ndef
          Bdef(TS,R,i,np)=Bdef(TS,R,i,np)+xi_pops(idef).*Bnew(TS,R,i,np,idef);
          Bdef_all(TS,R,i)=Bdef_all(TS,R,i)+xi_pops(idef).*Bnew(TS,R,i,np,idef);
          prop(idef)=Bshannon(TS,R,i,np,idef)./Btot(TS,R,i,np);
          Shannon(TS,R,i,np)=Shannon(TS,R,i,np)+prop(idef).*log(prop(idef));
          end
          Shannon(TS,R,i,np)=-Shannon(TS,R,i,np)./log(ndef);
          Bdef(TS,R,i,np)=Bdef(TS,R,i,np)./Btot(TS,R,i,np);
          end
           Bdef_all(TS,R,i)=Bdef_all(TS,R,i)./Btot_size(TS,R,i);
           Shannon_size(TS,R,i)=-Shannon_size(TS,R,i)./log(npmax);
           end %seasonal cycle
         end %shape trade-off
         if (~seasonal)
        Bdef(Btot<0.1)=NaN; 
        Shannon(Btot<0.1)=NaN;
        Shannon_size(Btot_size<0.1)=NaN;
      
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      
           %Figures
                 
            itau=0;
     for to=1:4:length(tau_range)
         itau=itau+1;
         figure(1)
     subplot(3,length(N0_range),itau*length(N0_range)+(TS-length(N0_range)))
            plot(B_tau(to,:,1),'k--')
            hold on
             for np=1:npmax*ndef
                plot(B_tau(to,:,np+1))
                hold on
             end
            
 
 for fig=2:4
          figure(fig)
      subplot(4,length(N0_range),itau*length(N0_range)+(TS-length(N0_range)))
      if (fig==2)
      Biomass=squeeze(Btot(TS,R,to,:).*cCN.*1e-3);
      elseif (fig==3)
      Biomass=squeeze(Bdef(TS,R,to,:)); 
      else
      Biomass=squeeze(Shannon(TS,R,to,:));    
      end

      semilogx(V,squeeze(Biomass(:)),'ko')
    hold on  
xlabel('Size (\mugC)')
xlim([1e-8 1e2])
      if (fig==2)
      ylabel('Biomass (gC.m^{-3})')
      elseif (fig==3)
      ylabel('Defense parameter') 
        ylim([0 1])
      else
      ylabel('Shannon Index')
        ylim([0 1])
      end
 end %figures
     end %shape trade-off
     figure(5)
     subplot(1,length(N0_range),TS)
     plot(tau_range,squeeze(Shannon_size(TS,R,:)),'ko')
       set(gca, 'XTickLabel', get(gca,'XTick'))
       xlabel('Trade-off')
       ylabel('Shannon Index Total')
       ylim([0 1])
      xticks([0.1:0.3:1])
      ii=0;
      for tau_lab=1:3:length(tau_range)
         ii=ii+1;
         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
      end
      xticklabels(tau_labels)
     
     
  for fig=1:5
      figure(fig)
      set(gca,'FontSize',fsz)
      set(0,'defaultAxesFontName', 'Arial')
    set(0,'defaultTextFontName', 'Arial')
   
     if (TS==length(N0_range)) 
  pos = get(gcf, 'Position');
  set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
  set(gcf,'InvertHardcopy','on');
  set(gcf,'PaperUnits', 'inches');
  papersize = get(gcf, 'PaperSize');
  left = (papersize(1)- width)/2;
  bottom = (papersize(2)- height)/2;
  myfiguresize = [left, bottom, width, height];
  set(gcf,'PaperPosition', myfiguresize);
  if (fig==1)
  saveas(gcf,'Time_dynamic_10preys.jpg','jpg'); 
  elseif (fig==2)
  saveas(gcf,'Biomass_totale_10preys.jpg','jpg');
  elseif (fig==3)
  saveas(gcf,'Mean_def_10preys.jpg','jpg');  
  elseif (fig==4)
  saveas(gcf,'Shannon_per_size_10preys.jpg','jpg');
  else
  saveas(gcf,'Shannon_total_10preys.jpg','jpg'); 
  end
     end %if last subplot
      end % all figures
          end %seasonal cycle or not
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
%       for fig=1:3
%           figure(fig)
%       subplot(length(r_range),length(N0_range),R*length(N0_range)+(TS-length(N0_range)))
%       if (fig==1)
%       Biomass=squeeze(Btot(TS,R,:,:).*cCN);
%       elseif (fig==2)
%       Biomass=squeeze(Bdef(TS,R,:,:)); 
%       else
%       Biomass=squeeze(Shannon(TS,R,:,:));    
%       end
%       tau_2d=repmat(tau_range.',[1 length(V)]);
%       V_2d=repmat(V,[length(tau_range) 1]);
%       pcolor(tau_2d,V_2d,Biomass)
%       c=colorbar
%       caxis([0 N0])
%       set(gca,'Yscale','log')
%       if (fig==1)
%       ylabel(c,'Biomass (mgC.m^{-3})')
%       elseif (fig==2)
%       ylabel(c,'Defense parameter') 
%         caxis([0 1])
%       else
%       ylabel(c,'Shannon Index')
%         caxis([0 1])
%       end
%       ylabel('Size (\mug C)')
%       set(gca, 'XTickLabel', get(gca,'XTick'))
%       xlabel('Trade-off')
%      xticks([0.1:0.3:1])
%      ii=0;
%      for tau_lab=1:3:length(tau_range)
%         ii=ii+1;
%         tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
%      end
%      xticklabels(tau_labels)
%      shading flat
%       colormap(jet)
%      title(strcat(['r=',num2str(r),'-N=',num2str(N0)]))
%       set(gca,'FontSize',fsz)
%       set(0,'defaultAxesFontName', 'Arial')
%     set(0,'defaultTextFontName', 'Arial')
%    
%      if (R==length(r_range) & TS==length(N0_range)) 
%   pos = get(gcf, 'Position');
%   set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
%   set(gcf,'InvertHardcopy','on');
%   set(gcf,'PaperUnits', 'inches');
%   papersize = get(gcf, 'PaperSize');
%   left = (papersize(1)- width)/2;
%   bottom = (papersize(2)- height)/2;
%   myfiguresize = [left, bottom, width, height];
%   set(gcf,'PaperPosition', myfiguresize);
%   if (fig==1)
%   saveas(gcf,'Biomass_totale_10preys.jpg','jpg');
%   elseif (fig==2)
%   saveas(gcf,'Mean_def_10preys.jpg','jpg');  
%   else
%   saveas(gcf,'Shannon_10preys.jpg','jpg');    
%   end
%      end
%       end % 3 figures


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    else % optimization
      
         for i=1:length(tau_range)
             clear t B
            xi_old(1:npmax)=1;
            tau=tau_range(i);
            
            odefun = @(t,B) ode_test_defense_mixo(B);
            efun = @(t,B) odeevent(B);
            options=odeset('RelTol',1e-2,'AbsTol',1e-2,'Events',efun);
             [t,Bode]=ode45(odefun,[0:500],B0,options);
          Bend=squeeze(Bode(size(Bode,1),:).');
            options=optimoptions('fsolve','Display','Iter','StepTolerance',1e-12,'FunctionTolerance',1e-12,...
                'MaxFunctionEvaluations',1e4);
              [B_SS fval exitflag]=fsolve(@SS_test_defense_mixo,Bend,options);
              exitf(TS,R,i)=exitflag;
              
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
              figure(44+i)
             for np=1:npmax
                plot(Bode(:,np+1))
                hold on
             end
              saveas(gcf,strcat(['Optim_biomass_r=',num2str(r),'-N=',num2str(N0),'-tau=',num2str(i),'.jpg']),'jpg');
              clf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
         %     t_end=size(B,1);
        % biomass_t=squeeze(B(t_end,:).');
        [dBdt xi_optim(:)] = ode_test_defense_mixo(B_SS);
       
                  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               figure(55+i)
%              for np=1:npmax
%                 plot(xi_optim(:,np))
%                 hold on
%              end
%               saveas(gcf,strcat(['Optim_xi__r=',num2str(r),'-N=',num2str(N0),'-tau=',num2str(i),'.jpg']),'jpg');
%               clf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
             
        %     Beq=squeeze(B(size(B,1),:));
             Bnew(TS,R,i,:)=squeeze(B_SS(2:npmax+1)); 
             xi_opt_eq(TS,R,i,:)=xi_optim(:);
         end %tau
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     for fig=1:2
          figure(fig)
      subplot(length(r_range),length(N0_range),R*length(N0_range)+(TS-length(N0_range)))
      if (fig==1)
      Biomass=squeeze(Bnew(TS,R,:,:).*cCN);
      elseif (fig==2)
      Biomass=squeeze(xi_opt_eq(TS,R,:,:));     
      end
      tau_2d=repmat(tau_range.',[1 length(V)]);
      V_2d=repmat(V,[length(tau_range) 1]);
      pcolor(tau_2d,V_2d,Biomass)
      c=colorbar
      caxis([0 N0])
      set(gca,'Yscale','log')
   if (fig==1)
      ylabel(c,'Biomass (mgC.m^{-3})')
      elseif (fig==2)
      ylabel(c,'Optimized Defense parameter') 
        caxis([0 1])
      end
      ylabel('Size (\mug C)')
      set(gca, 'XTickLabel', get(gca,'XTick'))
      xlabel('Trade-off')
     xticks([0.1:0.3:1])
     ii=0;
     for tau_lab=1:3:length(tau_range)
        ii=ii+1;
        tau_labels(ii,:)=num2str(tau_range(tau_lab),'%.1f');
     end
     xticklabels(tau_labels)
     shading flat
      colormap(jet)
     title(strcat(['r=',num2str(r),'-N=',num2str(N0)]))
      set(gca,'FontSize',fsz)
      set(0,'defaultAxesFontName', 'Arial')
    set(0,'defaultTextFontName', 'Arial')
   
     if (R==length(r_range) & TS==length(N0_range)) 
  pos = get(gcf, 'Position');
  set(gcf, 'Position', [pos(1) pos(2) width*100, height*100]); %<- Set size
  set(gcf,'InvertHardcopy','on');
  set(gcf,'PaperUnits', 'inches');
  papersize = get(gcf, 'PaperSize');
  left = (papersize(1)- width)/2;
  bottom = (papersize(2)- height)/2;
  myfiguresize = [left, bottom, width, height];
  set(gcf,'PaperPosition', myfiguresize);
  if (fig==1)
  saveas(gcf,'Biomass_totale_optim.jpg','jpg');
  elseif (fig==2)
  saveas(gcf,'Optim_xi.jpg','jpg');   
  end
     end
      end % 2 figures 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   end %single/multiple preys/optimization defense
    end
end

%save results
if (competition & ~seasonal)
save('Results_def_addcost.mat','Bdef_all','Bdef','Btot_size',...
'Shannon_size','Btot','Shannon','Bnew')
 save('Fluxes_def_addcost.mat','mixotrophy','Eff','SP','PP','HTLflux')
elseif (~competition & ~opt_flag & ~seasonal)
      save('shannon_nodef.mat','Shannon','Biomass_tot_nodef','Biomass_nodef')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [x,isterm,dir] = odeevent2(B)

[dy] = ode_test_defense_mixo_with_light_cost_Naffinity(B);

x = norm(dy) - 1e-3;
%disp(x)

isterm = 1;

dir = 0;
end


