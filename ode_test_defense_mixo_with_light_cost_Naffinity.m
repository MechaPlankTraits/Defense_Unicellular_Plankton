%function [dBdt,mixo] = ode_test_defense_mixo(B) %for fsolve steady state resolution 
function  [dBdt,PP_tot,SP_tot,mort_HTL,TTE,mixo] = ode_test_defense_cost_Naffinity(B,t) %for ode23 and ode45


global r N0 P0 xi xi_range xi_pops ndef opt_flag competition tau seasonal
global phiN phiF phiL cL cN cF aF aN aL R0 m0 cCN mumax mHT mHT_quad
global V npmax m1 remin_rate xi_old light dil_rate


if (seasonal)
  if (roundn(t,0)==0)
      light_day=light(1);
      r_day=dil_rate(1);
  else
 light_day=light(roundn(t,0));
 r_day=dil_rate(roundn(t,0));
  end
end

%Retransformation en matrice 2 D car a ete modifier en vecteur lors de
%l'entree dans la fonction si ndef>1
if(ndef>1)
Bfunc(1,1)=B(1,1);
Bfunc(1,2:ndef)=0;
for np=1:npmax
  for idef=1:ndef
Bfunc(1+np,idef)=B(1+np+(idef-1)*(npmax+1));
  end
end
else
Bfunc=B;
end



pref=zeros(npmax,npmax);
    for ii=1:npmax
    for ij=1:npmax
        pref(ii,ij)= exp(-((log(V(ii)./(500.*V(ij)))).^2)./(2.*1.5));       
    end
    end
    
if (opt_flag)
    for ii=1:npmax
        %Calculate total food available %microg N.L-1
F(ii,1)=0;
for ij=1:npmax %ij: preys
F(ii,1)=F(ii,1)+xi_old(ij).*pref(ii,ij).*Bfunc(ij+1,1);
food(ii,ij)=xi_old(ij).*pref(ii,ij).*Bfunc(ij+1,1); %food pred ii sur proie ij avec defense jj
end

         alphaN(ii,1)=(cN.*V(ii).^(1/3)).*((aN.*phiN.*V(ii))./(aN.*phiN.*V(ii)+cN.*V(ii).^(1/3)));
         alphaF(ii,1)=(cF.*V(ii)).*((aF.*phiF)./(aF.*phiF+cF));
    
    JN(ii,1) = alphaN(ii).* Bfunc(1,1);
    JF(ii,1) = alphaF(ii).*F(ii);
        
    for ij=1:npmax % preys 
    JFF(ii,ij)=alphaF(ii).*food(ii,ij);
    end
       
 Jtot(ii,1)=JN(ii)+JF(ii);
    f(ii,1)=Jtot(ii)./(Jtot(ii)+xi_old(ii).^tau.*mumax.*(V(ii)./cCN));
   mu(ii,1)=xi_old(ii).^tau.*mumax.*f(ii);  
end % growth ii
 for ij=1:npmax %preys
     loss(ij,1)=0.0;
     for ii=1:npmax %predators 
         loss(ij,1)=loss(ij)+(JFF(ii,ij).*(1-f(ii)).*(Bfunc(ii+1,1)./(V(ii)./cCN)));
     end
 end
 
 load('X_N_dat.mat');load('X_F_dat.mat');load('X_G_dat.mat');load('X_tau_dat.mat');
 load('phi_D_opt.mat');
 
 diff_N=abs(Bfunc(1,1)-X_N);
 [val idx_N]=min(diff_N); 
 tau_range=[0.1:0.1:1];
  diff_tau=abs(tau-tau_range);
 [val idx_tau]=min(diff_tau);
 
 
 for ii=1:npmax
 diff_F=abs(F(ii)-X_F);
 [val idx_F]=min(diff_F);
 diff_G=abs(loss(ii)-X_G);
 [val idx_G]=min(diff_G);
     xi_new(ii,1)=opt_phiD(idx_N,idx_F,idx_G,idx_tau,ii);
 end %
 xi=xi_new;
end %opt_flag


% Plankton Growth
for ii=1:npmax
%Calculate total food available %microg N.L-1
F(ii,1)=0;
   for jj=1:ndef %defense strategy of the prey
for ij=1:npmax %ij: preys
F(ii,1)=F(ii,1)+(1-xi(ij,jj).^tau).*pref(ii,ij).*Bfunc(ij+1,jj);
food(ii,ij,jj)=(1-xi(ij,jj).^tau).*pref(ii,ij).*Bfunc(ij+1,jj); %food pred ii sur proie ij avec defense jj
end
   end
  alphaL(ii,1)=(cL.*V(ii).^(2/3)).*((aL.*phiL.*V(ii))./(aL.*phiL.*V(ii)+cL.*V(ii).^(2/3)));
  alphaN(ii,1)=(cN.*V(ii).^(1/3)).*((aN.*phiN.*V(ii))./(aN.*phiN.*V(ii)+cN.*V(ii).^(1/3)));
  alphaF(ii,1)=(cF.*V(ii)).*((aF.*phiF)./(aF.*phiF+cF));
  if (seasonal)
     JL(ii,1) = alphaL(ii).* light_day; 
  else
    JL(ii,1) = alphaL(ii).* light;
  end
    for jj=1:ndef
 %  JN(ii,jj) = (1-xi(ii,jj)).^tau.*alphaN(ii).* Bfunc(1,1);
   JN(ii,jj) = alphaN(ii).* Bfunc(1,1);
    JF(ii,jj) = alphaF(ii).*F(ii);
   % JF(ii,jj) = (1-xi(ii,jj)).^tau.*alphaF(ii).*F(ii);
    end
   
   
%     if (~competition & ~opt_flag)
%     if nargout>1
          for jj=1:ndef
      mixo(ii,jj)=JF(ii,jj)./(JF(ii,jj)+JN(ii,jj));
          end
%     end
%     end
 % Maximum consumption of each prey ij inMini microgN.d-1
  for jj=1:ndef %defense prey
    for ij=1:npmax % preys 
  %  JFF(ii,ij,jj)=(1-xi(ii,jj)).^tau.*alphaF(ii).*food(ii,ij,jj);
    JFF(ii,ij,jj)=alphaF(ii).*food(ii,ij,jj);
    end
  end
    
  
  % Requirement of a fixed C/N ratio
  for jj=1:ndef
 Cconsump(ii,jj)=JF(ii,jj)+max(0,(JL(ii)-R0.*V(ii))./cCN); %conversion to nitrogen 
 Nconsump(ii,jj)=JN(ii,jj)+JF(ii,jj);
 
    Jeff(ii,jj)=min(Cconsump(ii,jj),Nconsump(ii,jj)); % unit of nitrogen per day
 
 % Calculate the leakage of either C or N 
 if (Jeff(ii,jj)==Cconsump(ii,jj)) % N leakage
     Nleak(ii,jj)=Nconsump(ii,jj)-Cconsump(ii,jj); % N in excess
     Cleak(ii,jj)=0;
 else
     Nleak(ii,jj)=0;
     Cleak(ii,jj)=(Cconsump(ii,jj)-Nconsump(ii,jj)); %C in excess %unit of nitrogen
 end
  
%Cost on mumax 
%   f(ii,jj)=Jeff(ii,jj)./(Jeff(ii,jj)+(1-xi(ii,jj)).^tau.*mumax.*(V(ii)./cCN));
%   mu(ii,jj)=(1-xi(ii,jj)).^tau.*mumax.*f(ii,jj);
   
%Addition metabolic cost 
 Jcost(ii,jj)=Jeff(ii,jj)-mumax.*(V(ii)./cCN).*xi(ii,jj).^(1/tau);
 f(ii,jj)=Jcost(ii,jj)./(Jcost(ii,jj)+mumax.*(V(ii)./cCN));
 mu(ii,jj)=mumax.*f(ii,jj);

%Cost on affinitie(s)
 %   f(ii,jj)=Jeff(ii,jj)./(Jeff(ii,jj)+mumax.*(V(ii)./cCN));
 %   mu(ii,jj)=mumax.*f(ii,jj);
     end
    
    
    for jj=1:ndef %defenses
       PP(ii,jj)=max(0,(Cconsump(ii,jj)-JF(ii,jj)-Cleak(ii,jj)).*Bfunc(ii+1,jj)./(V(ii)./cCN)*(1-f(ii,jj)));
        Nuptake(ii,jj)=(JN(ii,jj)-Nleak(ii,jj)).*(1-f(ii,jj)).*Bfunc(ii+1,jj)./(V(ii)./cCN); %N
        SP(ii,jj)=JF(ii,jj).*Bfunc(ii+1,jj)./(V(ii)./cCN)*(1-f(ii,jj));
    end    
end % growth ii

for jj=1:ndef %defense prey
 for ij=1:npmax %preys
     loss(ij,jj)=0.0;
    for ji=1:ndef %defense pred
     for ii=1:npmax %predators 
         loss(ij,jj)=loss(ij,jj)+(JFF(ii,ij,jj).*(1-f(ii,ji)).*(Bfunc(ii+1,ji)./(V(ii)./cCN)));
     end
    end
 end
end

mort_HTL=0;
% Natural and higher trophic levels Mortality
for jj=1:ndef
    for ii=1:npmax
    mort(ii,jj)=m0+m1.*Bfunc(ii+1,jj);
    end %size classes 
     mort(npmax,jj)=mort(npmax,jj)+(1-xi(npmax,jj).^tau)*mHT;  
    mort(npmax-1,jj)= mort(npmax-1,jj)+(1-xi(npmax-1,jj).^tau)*mHT/1.2;
    mort(npmax-2,jj)=mort(npmax-2,jj)+(1-xi(npmax-2,jj).^tau)*mHT/2;

    mort_HTL=mort_HTL+(1-xi(npmax,jj).^tau).*mHT.*Bfunc(npmax+1,jj)+...
                      (1-xi(npmax-1,jj).^tau).*mHT/1.2.*Bfunc(npmax,jj)+...
                      (1-xi(npmax-2,jj).^tau).*mHT/2.*Bfunc(npmax-1,jj);   
    
%     mort(npmax,jj)=mort(npmax,jj)+(1-xi(npmax,jj).^tau)*mHT_quad.*Bfunc(npmax+1,jj);  
%     mort(npmax-1,jj)= mort(npmax-1,jj)+(1-xi(npmax-1,jj).^tau)*mHT_quad/1.2.*Bfunc(npmax,jj);
%     mort(npmax-2,jj)=mort(npmax-2,jj)+(1-xi(npmax-2,jj).^tau)*mHT_quad/2.*Bfunc(npmax-1,jj);
%                   
%    mort_HTL=mort_HTL+(1-xi(npmax,jj).^tau).*mHT_quad.*Bfunc(npmax+1,jj)^2+...
%                       (1-xi(npmax-1,jj).^tau).*mHT_quad.*Bfunc(npmax,jj)^2+...
%                       (1-xi(npmax-2,jj).^tau).*mHT_quad.*Bfunc(npmax-1,jj)^2;
end %defense


    mort_tot=sum(sum(mort.*Bfunc(2:npmax+1,:))); %mugN.L-1.d-1
    uptake_tot=sum(sum(Nuptake(:,:))); %mugN.L-1.d-1
    PP_tot=sum(sum(PP));
    SP_tot=sum(sum(SP));
    TTE=mort_HTL./PP_tot;
    
%Plankton biomass time variation
if (seasonal)
diff(2:npmax+1,1:ndef) = (mu - mort-r_day).*Bfunc(2:npmax+1,:)-loss+r_day.*P0;
diff(1,1)=r_day.*(N0-Bfunc(1,1))-uptake_tot+remin_rate*mort_tot; 
else
diff(2:npmax+1,1:ndef) = (mu - mort-r).*Bfunc(2:npmax+1,:)-loss+r.*P0;
diff(1,1)=r.*(N0-Bfunc(1,1))-uptake_tot+remin_rate*mort_tot;
end

if (ndef>1)
dBdt=reshape(diff, [(npmax+1)*ndef 1]);
else
    dBdt=diff;
end

if (opt_flag)
    if nargout>1
      xi_optim = xi; 
    end
 xi_old=xi;
end


